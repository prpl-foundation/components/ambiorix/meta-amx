SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/applications/amxrt.git;protocol=https;nobranch=1"

SRCREV = "v2.2.0"
SRCREV_kirkstone = "bd87d305082faf2e4d5531db84f538c737b1c6a2"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Data model runtime"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "amxrt"

DEPENDS += "libamxc libamxb libamxd libamxj libamxp libamxo libamxrt libevent yajl"
RDEPENDS_${PN} += "libamxc libamxb libamxd libamxj libamxp libamxo libamxrt libevent yajl"

FILES_${PN} += " \
    ${BINDIR}/${COMPONENT} \
    ${libdir}/amx/scripts/amx_init_functions.sh \
"



SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/applications/amx-cli.git;protocol=https;nobranch=1"
SRCREV = "v0.3.3"
SRCREV_kirkstone = "b1ad7569e4d503d847783ad0389cda02f98f6590"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Ambiorix interactive CLI"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=6985054d3f2d7dbde00e278406c8cda2"

COMPONENT = "amx-cli"


DEPENDS += "libamxc"
DEPENDS += "libamxp"
DEPENDS += "libamxt"
DEPENDS += "libamxm"
DEPENDS += "libamxo"
DEPENDS += "libamxj"
DEPENDS += "libevent"
DEPENDS += "yajl"

RDEPENDS_${PN} += "mod-ba-cli"

FILES_${PN} += "${BINDIR}/${COMPONENT}"
FILES_${PN} += "/etc/amx/cli/amx-cli.conf"
FILES_${PN} += "/etc/amx/cli/amx-cli.init"

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/amxlab/tui/applications/dmtui.git;protocol=https;nobranch=1"

SRCREV = "v0.3.3"
SRCREV_kirkstone = "24ec4ae25e000b8fae7c61af6345b12d616e2f14"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://001_select_objects_to_be_shown.patch"

SUMMARY = "Simple example application that creates a data model using the Ambiorix framework"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "dmtui"

DEPENDS += "libamxc libamxp libamxt libamxd libamxo libamxb libamxtui"
RDEPENDS:${PN} += "libamxc libamxp libamxt libamxd libamxo libamxb libamxtui"

FILES:${PN} += "/etc/amx/dmtui/dmtui.odl"
FILES:${PN} += "/usr/lib/amx/dmtui/dmtui.so"
FILES:${PN} += "${BINDIR}/dmtui"

DESCRIPTION = "OpenWrt system message/RPC bus"
HOMEPAGE = "https://git.openwrt.org/project/ubus.git"

LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://ubusd.c;beginline=1;endline=12;md5=1b6a7aecd35bdd25de35da967668485d"
SECTION = "base"
DEPENDS = "json-c libubox"

SRC_URI = "\
          git://git.openwrt.org/project/ubus.git;protocol=https;nobranch=1; \
          file://ubusd.service \
          "

SRC_URI_append_dunfell = "file://0001-lua5.3-support.patch"
SRC_URI_append_kirkstone = "file://0001-lua5.4-support.patch"

SRCREV = "d35df8adda873dc75d876f72b78e84db8cfa72ee"

ABI_VERSION = "20191227"

S = "${WORKDIR}/git"
IMAGE = "${WORKDIR}/image"

inherit cmake openwrt-lua openwrt-virtual-runtimes pkgconfig systemd

EXTRA_OECMAKE += "\
                -DCMAKE_INSTALL_LIBDIR:PATH=/lib \
                "

FILES_${PN} += "/usr/sbin/ubusd"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = "ubusd.service"
FILES_${PN} += "${systemd_unitdir}/system/ubusd.service"

do_install_append () {
    install -dm 0755 ${D}/sbin
    ln -s /usr/bin/ubusd ${D}/sbin/ubusd
    install -m 0755 ${WORKDIR}/build/ubusd  ${D}${bindir}
    install -m 0755 ${WORKDIR}/build/ubusd  ${D}${sbindir}
    install -D -m 0644 ${WORKDIR}/ubusd.service ${D}${systemd_unitdir}/system/ubusd.service

    install -m 0755 ${S}/lua/publisher.lua ${D}${bindir}
    install -m 0755 ${S}/lua/subscriber.lua ${D}${bindir}
    install -m 0755 ${S}/lua/test_client.lua ${D}${bindir}
    install -m 0755 ${S}/lua/test.lua ${D}${bindir}
}

PACKAGES =+ "\
            lib${PN}${ABI_VERSION} \
            lib${PN}-lua \
            lib${PN}-examples \
            ${PN}d \
            "

FILES_SOLIBSDEV = ""
FILES_lib${PN}${ABI_VERSION} += "/lib/libubus.so"
FILES_lib${PN}-lua += "${libdir}/lua/5.3/*"
FILES_lib${PN}-examples += "${bindir}/*.lua"
FILES_${PN}d += "${sbindir} ${base_sbindir}"
FILES_${PN} += "${bindir}"

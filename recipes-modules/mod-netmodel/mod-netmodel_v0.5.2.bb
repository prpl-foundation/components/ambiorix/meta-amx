SRC_URI = "git://gitlab.com/prpl-foundation/components/netmodel/modules/mod-netmodel.git;protocol=https;nobranch=1"

SRCREV = "v0.5.2"
SRCREV_kirkstone = "523cd8a8e1e67ffac93e8b6e26b6d36a5f074973"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Module to populate NetModel"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=6985054d3f2d7dbde00e278406c8cda2"

COMPONENT = "mod-netmodel"

DEPENDS += "libamxc libamxp libamxb libamxo libsahtrace"

FILES_${PN} += "${libdir}/amx/modules/${COMPONENT}${SOLIBSDEV}"
FILES_${PN} += "${libdir}/amx/modules/${COMPONENT}${SOLIBS}"

INSANE_SKIP:${PN} += "dev-so"

SUMMARY = "Bus Agnostic Interactive CLI"
DESCRIPTION = "Bus Agnostic Interactive Command Line Interface"

LICENSE = "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=6985054d3f2d7dbde00e278406c8cda2"

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/modules/amx_cli/mod-ba-cli.git;protocol=https;nobranch=1"

SRCREV = "v0.9.0"
SRCREV_kirkstone = "9ff97d41af48739eb3d6629301eea9a2caab5c8d"
S = "${WORKDIR}/git"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://001-mod-ba-cli-autoconnect.patch \
    file://002-mod-ba-cli-no-logo.patch \
    file://003-mod-ba-cli-configurable-timeout.patch \
    file://004-mod-ba-cli-invalid-path-check.patch \
    file://005-mod-ba-cli-rbus-variables.patch \
"

inherit pkgconfig config-ambiorix

COMPONENT = "mod-ba-cli"

DEPENDS += "libamxc libamxt libamxm libamxp libamxd libamxb libamxo libamxa"

do_install_append() {
    install -d ${D}/etc/amx/cli
    install -m 775 ${S}/config/rbus-cli.init ${D}/etc/amx/cli
    install -m 775 ${S}/config/rbus-cli.conf ${D}/etc/amx/cli
}

FILES_${PN} += "${libdir}/amx/amx-cli/${COMPONENT}.so"
FILES_${PN} += "/etc/amx/cli/rbus-cli.init"
FILES_${PN} += "/etc/amx/cli/rbus-cli.conf"

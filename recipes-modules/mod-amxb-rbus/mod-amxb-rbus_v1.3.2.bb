SUMMARY = "Amxb Rbus Backend"
SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_rbus.git;protocol=https;nobranch=1 \
            "
SRCREV = "796ddafb2c353b9b1745a4fd13c9ae201d7c0f5f"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "mod-amxb-rbus"

DEPENDS += "libamxc"
DEPENDS += "libamxp"
DEPENDS += "libamxb"
DEPENDS += "rbus"


RDEPENDS_${PN} += "libamxc"
RDEPENDS_${PN} += "libamxp"
RDEPENDS_${PN} += "libamxb"
RDEPENDS_${PN} += "rbus"

FILES_${PN} += "/usr/bin/mods/amxb/${COMPONENT}.so"
FILES_${PN} += "${LIBDIR}/libamxb_rbus${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/libamxb_rbus${SOLIBSDEV}"

# Ensure rbus.h is found during compilation
CFLAGS += " -isystem${STAGING_INCDIR}/rbus "

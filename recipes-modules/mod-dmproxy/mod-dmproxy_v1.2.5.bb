# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
# This code is subject to the terms of the BSD-2+SAH Patent license.
# See LICENSE file for more details.

SUMMARY = "Module wrapper to proxy"
DESCRIPTION = "Module wrapper for proxy"

LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=a705237d3056b8a8c89eb03485d722ce"
SRC_URI = "git://gitlab.com/prpl-foundation/components/core/modules/mod-dmproxy.git;protocol=https;nobranch=1"
           
SRCREV = "v1.2.5"
SRCREV_kirkstone = "6088d7eaf735d1cdec63aaebb47d960d36c64a7e"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix 

COMPONENT = "mod-dmproxy"


DEPENDS += "libamxc"
DEPENDS += "libamxp"
DEPENDS += "libamxo"
DEPENDS += "libamxd"
DEPENDS += "libamxb"

RDEPENDS_${PN} += "libamxc"
RDEPENDS_${PN} += "libamxp"
RDEPENDS_${PN} += "libamxo"
RDEPENDS_${PN} += "libamxd"
RDEPENDS_${PN} += "libamxb"

FILES_${PN} += "/usr/lib/amx/modules/mod-dmproxy.so"

SRC_URI = "git://gitlab.com/prpl-foundation/components/core/modules/mod-dmext.git;protocol=https;nobranch=1"
           
SRCREV = "v0.6.0"
SRCREV_kirkstone = "79765700309ad73508dde94f26d56a4e4672f5ee"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix 

SUMMARY = "Data model extension module"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=6985054d3f2d7dbde00e278406c8cda2"

COMPONENT = "mod-dmext"

DEPENDS += "libamxc libamxp libamxo libamxd libamxb"

FILES_${PN} += "${libdir}/amx/modules/${COMPONENT}${SOLIBSDEV}"
FILES_${PN} += "${libdir}/amx/modules/${COMPONENT}${SOLIBS}"

INSANE_SKIP:${PN} += "dev-so"

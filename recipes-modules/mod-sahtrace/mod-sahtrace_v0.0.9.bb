# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
# This code is subject to the terms of the BSD-2+SAH Patent license.
# See LICENSE file for more details.

SUMMARY = "Module wrapper to enable tracing and logging"
DESCRIPTION = "Module wrapper for a small and flexible library to enable tracing and logging"

LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=6bb6609ec7c25caf8b7b0eb6ed4480cf"

SRC_URI = "git://gitlab.com/soft.at.home/ambiorix/modules/mod-sahtrace.git;protocol=https;nobranch=1 \
           "

SRCREV = "v0.0.9"
SRCREV_kirkstone = "06560558f0a368f1612976f07817e092ef0207e1"
S = "${WORKDIR}/git"
inherit pkgconfig config-sah-trace

COMPONENT = "mod-sahtrace"

DEPENDS += "libsahtrace"
DEPENDS += "libamxc"
DEPENDS += "libamxo"
DEPENDS += "libamxd"

RDEPENDS_${PN} += "libsahtrace"
RDEPENDS_${PN} += "libamxc"
RDEPENDS_${PN} += "libamxo"
RDEPENDS_${PN} += "libamxd"

FILES_${PN} += "/usr/lib/amx/modules/mod-sahtrace.so"

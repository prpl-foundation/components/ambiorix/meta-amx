

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_ubus.git;protocol=https;nobranch=1"
SRCREV = "v3.1.17"
SRCREV_kirkstone = "6d1cca61f700647225e0e56adae1c21507346be7"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Ubus Backend"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=6985054d3f2d7dbde00e278406c8cda2"

COMPONENT = "mod-amxb-ubus"


DEPENDS += "libamxc"
DEPENDS += "libamxb"
DEPENDS += "libamxd"
DEPENDS += "ubus"

RDEPENDS_${PN} += "libamxc"
RDEPENDS_${PN} += "libamxb"
RDEPENDS_${PN} += "libamxd"
RDEPENDS_${PN} += "ubus"

FILES_${PN}-dev += "${INCLUDEDIR}/amxb_be_ubus/*.h"
FILES_${PN} += "/usr/bin/mods/amxb/${COMPONENT}.so"
FILES_${PN} += "${LIBDIR}/libamxb_ubus${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/libamxb_ubus${SOLIBSDEV}"
FILES_${PN} += "${BINDIR}/forward_ubus_client.sh"
FILES_${PN} += "${BINDIR}/forward_ubus_server.sh"

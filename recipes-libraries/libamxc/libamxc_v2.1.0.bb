SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc.git;protocol=https;nobranch=1"

SRCREV = "v2.1.0"
SRCREV_kirkstone = "2b1e9463b6a56fe873d2b1875917a73446c47397"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Libamxc is a library containing data containers, implemented in ansi C (C99)."
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxc"

FILES_${PN}-dev += "${INCLUDEDIR}/amxc/*.h"
FILES_${PN}-staticdev += "${LIBDIR}/${COMPONENT}.a"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

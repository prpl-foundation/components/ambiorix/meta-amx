SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxt.git;protocol=https;nobranch=1"

SRCREV = "v1.0.0"
SRCREV_kirkstone = "f688362448977fce6d48cdc3543a84546730cc15"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Common patterns implementation"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxt"

DEPENDS += "libamxc libamxp"
RDEPENDS_${PN} += "libamxc libamxp"

FILES_${PN}-dev += "${INCLUDEDIR}/amxt/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

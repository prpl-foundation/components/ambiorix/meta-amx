# SPDX-License-Identifier: BSD-2-Clause-Patent
# SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
# This code is subject to the terms of the BSD-2+SAH Patent license.
# See LICENSE file for more details.

SUMMARY = "Small and flexible library to enable tracing and logging"
DESCRIPTION = "This library is created to provide a simple and flexible system wide tracing and debuging functionality. Allow dynamic trace control including tracezones" 

LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

SRC_URI = "git://gitlab.com/prpl-foundation/components/core/libraries/libsahtrace.git;protocol=https;nobranch=1"

SRCREV_kirkstone = "435fdd2e774d804c4c95c39175fb885043a65837"
SRCREV = "v1.15.0"
S = "${WORKDIR}/git"
inherit pkgconfig config-sah-trace

COMPONENT = "libsahtrace"

FILES_SOLIBSDEV = ""

FILES_${PN}-dev += "${includedir}/debug/*.h"
FILES_${PN} += "/lib/${COMPONENT}${SOLIBS}"
FILES_${PN} += "/lib/${COMPONENT}${SOLIBSDEV}"

INSANE_SKIP:${PN} += "dev-so"

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo.git;protocol=https;nobranch=1"

SRCREV = "v5.0.2"
SRCREV_kirkstone = "f5f1dcc370e794fe3e70554176317d756d650b0a"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Ambiorix Object Definition Language library"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxo"

DEPENDS += "libamxc libamxp libamxd libamxs bison-native flex-native"
RDEPENDS_${PN} += "libamxc libamxp libamxd libamxs"

FILES_${PN}-dev += "${INCLUDEDIR}/amxo/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj.git;protocol=https;nobranch=1"

SRCREV = "v1.0.3"
SRCREV_kirkstone = "45cd10940c480fbcf0ff50c852a8ddac3774a320"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "JSON parser & generator using yajl and libamxc variants"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxj"

DEPENDS += "libamxc yajl"
RDEPENDS_${PN} += "libamxc yajl"

FILES_${PN}-dev += "${INCLUDEDIR}/amxj/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

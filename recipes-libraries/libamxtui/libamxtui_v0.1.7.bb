SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/amxlab/tui/libraries/libamxtui.git;protocol=https;nobranch=1"

SRCREV = "v0.1.7"
SRCREV_kirkstone = "6aca2d1112f3eec471244bdb75464bb6b0f67f81"

S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Libamxtui is a library that provides terminal user interface widgets based on ncurses."
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxtui"

DEPENDS += "libamxc libamxt libamxp ncurses"
RDEPENDS:${PN} += "libamxc libamxt libamxp ncurses"

FILES:${PN}-dev += "${INCLUDEDIR}/amxtui/*.h"
FILES:${PN}-dev += "${INCLUDEDIR}/amxtui/ctrl/*.h"
FILES:${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES:${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb.git;protocol=https;nobranch=1"

SRCREV = "v4.11.1"
SRCREV_kirkstone = "e3aea113264f3f0a905a7843defd70cfa298de2b"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Bus agnostic C API (mediator)"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxb"

DEPENDS += "libamxc libamxp libamxd uriparser"
RDEPENDS_${PN} += "libamxc libamxp libamxd uriparser"

FILES_${PN}-dev += "${INCLUDEDIR}/amxb/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

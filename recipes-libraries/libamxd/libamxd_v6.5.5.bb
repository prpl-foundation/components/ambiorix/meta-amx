SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd.git;protocol=https;nobranch=1"

SRCREV = "v6.5.5"
SRCREV_kirkstone = "446436a746112ece15363fb68864ec7b064f3f4e"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Data model C-API"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxd"

DEPENDS += "libamxc libamxp"
RDEPENDS_${PN} += "libamxc libamxp"

FILES_${PN}-dev += "${INCLUDEDIR}/amxd/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

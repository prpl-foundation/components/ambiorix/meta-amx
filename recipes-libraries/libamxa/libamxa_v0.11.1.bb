SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxa.git;protocol=https;nobranch=1"

SRCREV = "3d28edc5fde329ccad86619f8aeb0a7e47cc6802"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Access control verification"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxa"

DEPENDS += "libamxc libamxj libamxd libamxb"
RDEPENDS_${PN} += "libamxc libamxj libamxd libamxb"

FILES_${PN}-dev += "${INCLUDEDIR}/amxa/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

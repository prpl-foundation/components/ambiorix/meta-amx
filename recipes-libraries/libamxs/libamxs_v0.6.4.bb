SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxs.git;protocol=https;nobranch=1"

SRCREV = "v0.6.4"
SRCREV_kirkstone = "0796b8d1bf11562d9bda03590a94e2bae3cd0083"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Data Model Synchronization C API"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxs"

DEPENDS += "libamxc libamxp libamxb"
RDEPENDS_${PN} += "libamxc libamxp libamxb"

FILES_${PN}-dev += "${INCLUDEDIR}/amxs/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"


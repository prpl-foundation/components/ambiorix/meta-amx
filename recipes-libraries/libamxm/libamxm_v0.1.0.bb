SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm.git;protocol=https;nobranch=1"

SRCREV = "v0.1.0"
SRCREV_kirkstone = "8e881e38b76fee92cd9b30aabf531d79be10c489"
S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "modularity api, simplifies creation of add-ons (plug-ins, modules)"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxm"

DEPENDS += "libamxc libamxp"
RDEPENDS_${PN} += "libamxc libamxp"

FILES_${PN}-dev += "${INCLUDEDIR}/amxm/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"

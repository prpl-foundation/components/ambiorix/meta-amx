SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxrt.git;protocol=https;nobranch=1"

SRCREV = "8ea9686f3c8d23394618d25cac033ce581d4b765"

S = "${WORKDIR}/git"
inherit pkgconfig config-ambiorix

SUMMARY = "Common patterns implementation"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "libamxrt"

DEPENDS += "libamxc libamxp libamxd libamxb libamxo libamxj libcap-ng yajl libevent"
RDEPENDS_${PN} += "libamxc libamxp libamxd libamxb libamxo libamxj libcap-ng yajl libevent"

FILES_${PN}-dev += "${INCLUDEDIR}/amxrt/*.h"
FILES_${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES_${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"


# Class for meta-sah-trace building configuration

EXTRA_OEMAKE += "DEST=${D} \
                 D=${D} \
                 PREFIX=${prefix} \
                 LIBDIR=${libdir} \
                 BINDIR=${bindir} \
                 INCLUDEDIR=${includedir} \
                 "

do_install() {
        oe_runmake install
}
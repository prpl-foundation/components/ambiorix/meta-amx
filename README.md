# meta-amx
External yocto layer for Prpl Ambiorix components and dependencies.  
Runs ubusd systemd service as default ambiorix IPC method.

### Usage

Add this layer to an existing RDK-B yocto environment.  

Configure the layer into the build by adding the following into `conf/bblayers.conf`
```
BBLAYERS += "${RDKROOT}/meta-amx"
```  
List all recipes to verify that the layer is added correctly.
```
$ bitbake -s
```
